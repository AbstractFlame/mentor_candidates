from __future__ import unicode_literals

from django.db import models

# Create your models here.

class Mentor(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=200)
    email = models.EmailField()
    description = models.TextField(null = True)

class Opinion(models.Model):
    name = models.CharField(max_length=200)
    description = models.TextField()
    mentor = models.ForeignKey(Mentor, on_delete=models.CASCADE)    